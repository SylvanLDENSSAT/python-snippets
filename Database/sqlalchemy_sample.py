"""
pip install sqlalchemy
"""
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Model = declarative_base()
engine = create_engine('sqlite:///:memory:')
# create a configured "Session" class
Session = sessionmaker(bind=engine)
# create a Session
session = Session()

class User(Model):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    passwd = Column(String, nullable=False)

    def __str__(self):
        n = len(self.passwd)
        return "User> login: %s ; passwd: %s"%(self.name, n*'*')

Model.metadata.create_all(engine)

pierre = User(name='pierre', passwd='1234')
paul = User(name='paul', passwd='0000')
jacques = User(name='jacques', passwd='1234')

session.add_all((pierre, paul, jacques))
session.commit()

users = session.query(User).all()
for user in users:
    print(user)
