import os.path
from flask import Flask, request, render_template, redirect
from werkzeug.utils import secure_filename

# instanciate flask app
app = Flask(__name__)

IMAGE_FOLDER = 'static/image'

# serve a simple form asking to upload image
@app.route('/')
def index():
    # send index.html from folder templates/
    return render_template('index.html')

# handle image sent by the previous form
@app.route('/api/upload_image', methods=['POST'])
def resource_handle_image():
    image = request.files.get('myimage')
    if image:
        filename = secure_filename(image.filename)
        image.save( os.path.join(IMAGE_FOLDER, filename) )
    return redirect('/static/image/%s'%filename)

# run flask web server on http://localhost:5000
app.run(host='localhost', port=5000)
