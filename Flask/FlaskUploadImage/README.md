# Upload image (Flask)

> Simple snippet to upload an image using an HTML form and handle it server side.


## How it works

- images are uploaded in folder *static/image*
- html templates are stored in *templates/* folder
