# coding: utf-8

import socket

hote = "localhost"
port = 5000

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.connect((hote, port))
print("Connection on {}".format(port))

socket.send(bytes("Hey my name is Olivier!", "utf-8"))

print("Close")
socket.close()
