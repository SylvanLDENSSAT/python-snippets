# coding: utf-8

import socket

host = 'localhost'
port = 5000
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.bind((host, port))

print("waiting for client on {}:{}".format(host, port))
while True:
        socket.listen(5)
        client, address = socket.accept()
        print("{} connected".format( address ) )

        response = client.recv(255)
        if response != "":
                print(response)

print("Close")
client.close()
stock.close()
