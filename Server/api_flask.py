import json
from flask import Flask, request, abort
from random import randint

app = Flask(__name__)
todolist = {}
rand_id = lambda: randint(0, 10000000)


@app.route('/api/todos')
def todo_list():
    return json.dumps(todolist)


@app.route('/api/todo/<int:task_id>')
def task(task_id=None):
    return json.dumps(todolist.get(task_id))


@app.route('/api/todos', methods=['POST'])
def create_task():
    # select the form in request.form, request.args, request.get_json
    form = request.get_json()
    # pick value from sended form
    title = form.get('title')
    date = form.get('date')
    # generate fake id
    new_id = rand_id()
    # add task to the list
    todolist[new_id] = {
        'title': title,
        'date': date
    }
    # return the newly created task
    return json.dumps(todolist[new_id])


@app.route('/api/todo/<int:task_id>', methods=['PUT'])
def modify_task(task_id=None):
    # select the form in request.form, request.args, request.get_json
    form = request.get_json()
    # pick value from sended form
    title = form.get('title')
    date = form.get('date')
    # update existing task
    if todolist.get(task_id):
        task = todolist[task_id]
        task['title'] = title
        task['date'] = date
        return json.dumps(task)
    else:
        return abort(404)


@app.route('/api/todo/<int:task_id>', methods=['DELETE'])
def delete_task(task_id=None):
    # given an id, delete associated task
    if todolist.get(task_id):
        del todolist[task_id]
        return json.dumps(True)
    else:
        return abort(404)


app.run(host='localhost', port=5000)
