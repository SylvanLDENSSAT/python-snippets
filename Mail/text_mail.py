"""
Send  a very simple mail using SMTP protocol
"""
import socket
import smtplib
from email.message import EmailMessage

host = 'smtp.gmail.com'
port = 587
sender = '<Email>'
passwd = '<Password>'

subject = 'A beautiful subject'
dest = ['sledeunf@gmail.com']
message = "Hello world!"


if __name__ == '__main__':
    server = smtplib.SMTP(host, port)
    # socket.gaierror: [caught] name or service not known")
    server.ehlo()

 
    server.starttls()
    server.login(sender, passwd)

    msg = EmailMessage()
    msg.set_content(message)

    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = ",".join(dest)

    server.send_message(msg)  

    server.close()
