from datetime import date, timedelta

if __name__ == '__main__':
    # get current date
    now = date.today()

    # create a time difference of 1 day
    a_day = timedelta(days=1)

    # compute yesterday's object
    yesterday = now - a_day

    # convert date object to a human readable string
    print(yesterday.strftime('%d/%m/%Y'))
 